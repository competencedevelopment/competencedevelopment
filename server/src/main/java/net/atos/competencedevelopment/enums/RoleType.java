package net.atos.competencedevelopment.enums;

public enum RoleType {
    ADMIN,
    USER,
    COMP_MANAGER
}
