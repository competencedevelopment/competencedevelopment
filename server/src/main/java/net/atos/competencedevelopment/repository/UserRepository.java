package net.atos.competencedevelopment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import net.atos.competencedevelopment.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
    
}
