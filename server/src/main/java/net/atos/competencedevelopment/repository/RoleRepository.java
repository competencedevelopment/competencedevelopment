package net.atos.competencedevelopment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import net.atos.competencedevelopment.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{
    
}
