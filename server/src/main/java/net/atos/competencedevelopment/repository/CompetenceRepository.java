package net.atos.competencedevelopment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import net.atos.competencedevelopment.model.Competence;

public interface CompetenceRepository extends JpaRepository<Competence, Long>{
    
}
