package net.atos.competencedevelopment.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class TestController {

    @Value("${profile}")
    private String profile;
    public final static String CLIENT_ORIGIN = "http://localhost:8090"; 
    @CrossOrigin(origins = CLIENT_ORIGIN )
    @RequestMapping("/")
    public String test() {
        return "Welcome to the Atos Competences Catalog server side";
    }

    @RequestMapping("/profile")
    public String name() {
        return profile;
    }
}
